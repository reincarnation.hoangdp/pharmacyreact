import {BehaviorSubject} from "rxjs";

const subscriber = new BehaviorSubject(null);

const ShareService = {
    sendMessages: (msg) => subscriber.next(msg),
    clearMessages: () => {
        subscriber.next(null)
        subscriber.complete()
    },
    getMessage: () => subscriber.asObservable()
}

export {
    ShareService,
    subscriber
}