import React from "react";
import Layout from "antd/es/layout/layout";

const {Footer} = Layout;
export default class AppFooter extends React.Component {
    render() {
        return (
            <Footer style={{
                textAlign: 'center',
                maxHeight: '70px',
                minHeight: '70px',
                height: '70px',
            }}>Ant
                Design ©2018 Created by Ant UED</Footer>
        );
    }
}