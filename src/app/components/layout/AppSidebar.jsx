import React from "react";
import {Layout, Menu} from "antd";
import {FileOutlined, TeamOutlined} from "@ant-design/icons";
import {DoctorIco, HomeIco, ProductIco} from "../../constants/custom-icons-constant";
import {ShareService} from "../../services/ShareService";
import '../../../assets/style/layout/sidebar.scss';

const {Sider} = Layout;
const {SubMenu} = Menu;
export default class AppSidebar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collapsed: this.props.collapsed,
        };
    }

    componentDidMount() {
        ShareService.getMessage().subscribe(msg => {
            if (msg) {
                switch (msg.key) {
                    case 'collapsedSidebar':
                        console.log(msg.value)
                        this.setState({
                            collapsed: msg.value
                        }, () => {

                        });
                        break;
                    default:
                        break;
                }
            }
        });
    }

    componentWillUnmount() {
        this.setState = (prevState, callback) => {
        };
        ShareService.clearMessages()
    }

    render() {
        return (
            <Sider className={"sidebar"} trigger={null} collapsible collapsed={this.state.collapsed}>
                <div className="logo">
                    <DoctorIco className={"logo-icon"}/>
                    <span className={"logo-title"}>quản lý bán thuốc</span>
                </div>
                <Menu className="main-menu" theme="dark" mode="inline">
                    <Menu.Item key="1" icon={<HomeIco/>}>
                        Trang chủ
                    </Menu.Item>
                    <SubMenu className={"submenu-sidebar"} key="sub1" icon={<ProductIco/>} title="Hàng hoá">
                        <Menu.Item key="3">Tom</Menu.Item>
                        <Menu.Item key="4">Bill</Menu.Item>
                        <Menu.Item key="5">Alex</Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub2" icon={<TeamOutlined/>} title="Team">
                        <Menu.Item key="6">Team 1</Menu.Item>
                        <Menu.Item key="7">Team 2</Menu.Item>
                        <Menu.Item key="8">Team 1</Menu.Item>
                        <Menu.Item key="9">Team 2</Menu.Item>
                        <Menu.Item key="10">Team 1</Menu.Item>
                        <Menu.Item key="11">Team 2</Menu.Item>
                        <Menu.Item key="12">Team 1</Menu.Item>
                        <Menu.Item key="13">Team 2</Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub3" icon={<TeamOutlined/>} title="Team">
                        <Menu.Item key="21">Team 1</Menu.Item>
                        <Menu.Item key="34">Team 2</Menu.Item>
                        <Menu.Item key="66">Team 1</Menu.Item>
                        <Menu.Item key="456">Team 2</Menu.Item>
                        <Menu.Item key="4563">Team 1</Menu.Item>
                        <Menu.Item key="333">Team 2</Menu.Item>
                        <Menu.Item key="4343">Team 1</Menu.Item>
                        <Menu.Item key="23232">Team 2</Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub4" icon={<TeamOutlined/>} title="Team">
                        <Menu.Item key="454">Team 1</Menu.Item>
                        <Menu.Item key="323">Team 2</Menu.Item>
                        <Menu.Item key="112">Team 1</Menu.Item>
                        <Menu.Item key="954">Team 2</Menu.Item>
                        <Menu.Item key="235">Team 1</Menu.Item>
                        <Menu.Item key="4356">Team 2</Menu.Item>
                        <Menu.Item key="6325">Team 1</Menu.Item>
                        <Menu.Item key="7944">Team 2</Menu.Item>
                    </SubMenu>
                    <Menu.Item key="5674" icon={<FileOutlined/>}>
                        Files
                    </Menu.Item>
                </Menu>
            </Sider>
        );
    }
}

/*STYLE*/
