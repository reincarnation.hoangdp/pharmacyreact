import React from "react";
import Layout from "antd/es/layout/layout";
import {Breadcrumb} from "antd";

const {Content} = Layout;
export default class AppContent extends React.Component {
    render() {
        return (
            <Content style={{margin: '0 16px',}}>
                <Breadcrumb style={{margin: '16px 0'}}>
                    <Breadcrumb.Item>User</Breadcrumb.Item>
                    <Breadcrumb.Item>Bill</Breadcrumb.Item>
                </Breadcrumb>
                <div className="app-content site-layout-background" style={layoutStl}>
                    Bill is a cat.
                </div>
            </Content>
        );
    }
}

/*STYLE*/
const layoutStl = {padding: 24, height: '100vh', background: '#fff'};