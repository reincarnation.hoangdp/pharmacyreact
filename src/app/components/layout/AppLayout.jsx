import React from "react";
import Layout from "antd/es/layout/layout";
import AppSidebar from "./AppSidebar";
import AppHeader from "./AppHeader";
import AppContent from "./AppContent";
import '../../../assets/style/layout/layout.scss';
import AppFooter from "./AppFooter";


export default class AppLayout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    render() {
        return (
            <Layout className={"app-layout"}>
                <AppSidebar collapsed={this.state.collapsed}/>
                <Layout style={{minHeight: '100vh'}}>
                    <AppHeader collapsed={this.state.collapsed}/>
                    <AppContent/>
                    <AppFooter/>
                </Layout>
            </Layout>
        );
    }
}

/*STYLE*/