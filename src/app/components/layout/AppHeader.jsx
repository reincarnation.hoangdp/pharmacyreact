import React from "react";
import {Col, Layout, Menu, message, Row} from "antd";
import {ShareService} from "../../services/ShareService";
import {AccountIco, CloseIco, MenuIco, SellIco, SettingIco, SignOutIco} from "../../constants/custom-icons-constant";
import {UserOutlined} from "@ant-design/icons";
import Avatar from "antd/es/avatar/avatar";
import '../../../assets/style/layout/header.scss';

const {SubMenu} = Menu;
export default class AppHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collapsed: this.props.collapsed,
        };
    }

    toggle = (event) => {
        event.preventDefault();
        this.setState({collapsed: !this.state.collapsed}, () => {
            console.log(this.state.collapsed)
            ShareService.sendMessages(Object.assign({
                key: 'collapsedSidebar',
                value: this.state.collapsed
            }))
        });
    };

    handleMenuClick = (e) => {
        message.info('Click on menu item.').then(r => {
        });
        console.log('click', e);
    }

    handleButtonClick = (e) => {
        message.info('Click on left button.').then(r => {
        });
        console.log('click left button', e);
    }

    menu = (
        <Menu onClick={(event) => this.handleMenuClick(event)}>
            <Menu.Item key="1" icon={<UserOutlined/>}>
                Thông tin tài khoản
            </Menu.Item>
            <Menu.Item key="2" icon={<UserOutlined/>}>
                Đăng xuất
            </Menu.Item>
        </Menu>
    );

    render() {
        return (
            <Header className="app-header" style={{display: 'block'}}>
                <Row>
                    <Col span={8}>
                        <div style={{flexGrow: '0'}} className={"toggleTrigger"}
                             onClick={(event) => this.toggle(event)}>
                            <ToggleIcon collapsed={this.state.collapsed}/>
                        </div>
                    </Col>
                    <Col span={16}>
                        <Row justify="end">
                            <Col span={24}>
                                <Menu className={"header-right-menu"}
                                      triggerSubMenuAction={"click"}
                                      style={{display: 'flex', flexDirection: 'row', justifyContent: 'flex-end'}}
                                      theme={"dark"} mode="horizontal"
                                      defaultSelectedKeys={['2']}>
                                    <Menu.Item key="1" icon={<SellIco/>} className={"header-right-menu-item"}>Bán
                                        hàng</Menu.Item>
                                    <Menu.Item key="2" icon={<SettingIco/>} className={"header-right-menu-item"}>Cài
                                        đặt</Menu.Item>
                                    <SubMenu className={"right-menu"} key="SubMenu" title={
                                        <>
                                            <span style={{margin: '0 10px'}}>Phan Hoàng</span>
                                            <Avatar icon={<UserOutlined/>}/>
                                        </>
                                    }>
                                        <Menu.Item icon={<AccountIco/>} className={"header-right-menu-item"}
                                                   key="setting:1">Tài khoản</Menu.Item>
                                        <Menu.Item icon={<SignOutIco/>} className={"header-right-menu-item"}
                                                   key="setting:2">Đăng xuất</Menu.Item>
                                    </SubMenu>
                                </Menu>
                            </Col>
                        </Row>
                    </Col>
                </Row>


            </Header>
        );
    }
}

/**/
const {Header} = Layout;
const ToggleIcon = ({collapsed}) => {
    return collapsed ? <MenuIco/> : <CloseIco/>;
};
