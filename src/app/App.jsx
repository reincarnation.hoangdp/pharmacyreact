import '../assets/style/App.scss';
import AppLayout from "./components/layout/AppLayout";
import 'antd/dist/antd.min'

function App() {
    return (
        <div className="App">
            <AppLayout />
        </div>
    );
}

export default App;
